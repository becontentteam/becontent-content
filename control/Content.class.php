<?php

namespace becontent\content\control;

use becontent\core\control\Settings as Settings;
use becontent\beContent as beContent;
use becontent\skin\Skinlet as Skinlet;

class Content {
	/**
	 *
	 * @var string
	 */
	private $resourceClassifier;
	/**
	 *
	 * @var array
	 */
	private $criteria = array ();
	/**
	 * 
	 * @var becontent\skin\presentation\Skinlet
	 */
	private $skinlet;
	
	/**
	 */
	public function __construct($resourceClassifier, $criteria = null) {
		if (isset ( $criteria )) {
			$this->criteria = $criteria;
		}
		if (isset ( $resourceClassifier )) {
			$this->resourceClassifier = $resourceClassifier;
		}
	}
	/**
	 */
	public function get() {
		
		$turnbackHtml = "La risorsa richiesta non &egrave; stata trovata";
		
		$requestedResources;
		
		if (isset ( $this->resourceClassifier )) {
			$requestedResources = beContent::getInstance ()->findResources ( $this->resourceClassifier, $this->criteria );
		}
		
		if (is_array ( $requestedResources ))
			if (file_exists ( Settings::getSkin () . '/' . $this->resourceClassifier . '_single.html' ) && count ( $requestedResources ) == 1) {
				$this->skinlet = new Skinlet ( 'single/' . $this->resourceClassifier . '_single' );
				$this->skinlet->setContent ( "instance", $requestedResources );
				$turnbackHtml = $this->skinlet->get ();
			} else {
				if (file_exists ( Settings::getSkin () . '/' . $this->resourceClassifier . '_multiple.html' ) && count ( $requestedResources ) > 1) {
					$this->skinlet = new Skinlet ( 'single/' . $this->resourceClassifier . '_multiple' );
					$this->skinlet->setContent ( "instances", $requestedResources );
					$turnbackHtml = $this->skinlet->get ();
				} else {
					$turnbackHtml = "Non esiste la skin per la risorsa richiesta, risorse: <br>" . json_encode ( $requestedResources );
				}
			}
		return $turnbackHtml;
	}
}

?>